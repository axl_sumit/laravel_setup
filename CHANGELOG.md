

## [1.4.0](https://gitlab.com/axl_sumit/laravel_setup/compare/1.3.0...1.4.0) (2024-01-02)


### Features

* file uploaded ([e1b135a](https://gitlab.com/axl_sumit/laravel_setup/commit/e1b135a6ddb9b654c3f9df1f45aec92bc8538300))

## 1.3.0 (2023-12-29)


### Features

* testing for version release ([83cdcf9](https://gitlab.com/axl_sumit/laravel_setup/commit/83cdcf9a35e1933e4181debf8f5acfe657a838e5))
* testing standard version ([263911b](https://gitlab.com/axl_sumit/laravel_setup/commit/263911be6835d4f1af39e75dfa6eea65b2987457))
* testing standard version ([b97eac1](https://gitlab.com/axl_sumit/laravel_setup/commit/b97eac1a77e7a8f82a6e0139b2471e78d06e3aaa))
* testing standard version ([25a022e](https://gitlab.com/axl_sumit/laravel_setup/commit/25a022ea01005f2bc1c17a1d79b08249a7200f17))
* testing standard version ([ce8fa48](https://gitlab.com/axl_sumit/laravel_setup/commit/ce8fa480729be67c3ed840aba9d361fce1da5586))
* testing standard version ([bb0ed95](https://gitlab.com/axl_sumit/laravel_setup/commit/bb0ed95f29507cad99a4c769ae46f987b7cd34fb))
* testing standard version ([3ff8af1](https://gitlab.com/axl_sumit/laravel_setup/commit/3ff8af1ffc0ea1e6f1e63dc8c421e14333cbbc61))
* testing standard version ([43706d6](https://gitlab.com/axl_sumit/laravel_setup/commit/43706d6aaf2aeaf9ce89cbb447636b1eb58ff2e0))
* testing standard version ([2956b58](https://gitlab.com/axl_sumit/laravel_setup/commit/2956b58419ad4c85c5cd250f55872e7dc401bd20))
* testing standard version ([cf8d3a9](https://gitlab.com/axl_sumit/laravel_setup/commit/cf8d3a9be2d16b031890b8396ea6ddba73e18c0d))
* testing standard version ([2ecbf9b](https://gitlab.com/axl_sumit/laravel_setup/commit/2ecbf9b8ec3d3cda93b6248729265b86081eb2eb))
